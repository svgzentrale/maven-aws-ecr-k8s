FROM maven:3.9-eclipse-temurin-21 as builder

RUN apt-get update
# set locales with utf8, is necessary vor auth act generator
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen en_US.UTF-8

# Install basic packages
RUN apt-get install -y --no-install-recommends jq curl git coreutils openssl bash openssh-client software-properties-common

#kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

# helm
# RUN apt-get install -y --no-install-recommends openssl bash
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh && ./get_helm.sh

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# azure cli
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

############ install python 3.12 ###############
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update && \
    apt install -y python3.12
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.12 1
RUN apt-get install -y python3-pip

# python modules installation
RUN pip3 install --no-cache-dir --break-system-packages requests \
    && pip3 install --no-cache-dir --break-system-packages pylint \
    && pip3 install --no-cache-dir --break-system-packages PyYAML

############ multi stage node ##################
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get update && apt-get install -y nodejs \
    && apt-get clean
RUN npm install -g yarn --force

############ set env ###########################
ENV JAVA_HOME=/opt/java/openjdk \
    PATH="/opt/java/openjdk/bin:$PATH"

############ install sonar-scanner #############
RUN yarn global add sonarqube-scanner

############ install ssh #######################
# RUN apt-get update && apt-get install -y --no-install-recommends openssh-client


############ check versions ####################
RUN python3 --version
RUN mvn -v
RUN java -version
RUN helm version
RUN yarn --version
RUN ssh -V
RUN node -v
RUN npm -v

# double check locales with this output, all variables must be set
#RUN locale

LABEL org.opencontainers.image.authors="a.bacher@svg.de, a.abba@svg.de"

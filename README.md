# maven-aws-ecr-k8s - The bitbucket pipeline build docker image for svg-monorepo

## 2024-11-27

Updated for Java 21, rebuild version 1.15-mono

This image is used in the build process.
It is not used to run the service in the deployment (pod), see s2i fabric8 image.

Docker Hub **was** connected to this repo.
Automatic build is no longer triggered because of docker's new licensing policy.

## HowTo build and push this to docker hub as svgdev/maven-aws-ecr-k8s

Build and test the image

    docker build --platform linux/amd64 -t svgdev/maven-aws-ecr-k8s:tagname .
    docker run svgdev/maven-aws-ecr-k8s:tagname


Login to docker hub via 

    docker login docker.io

or use login via Docker Desktop App.

Then

    docker push svgdev/maven-aws-ecr-k8s:tagname


## Examples

    docker build -t svgdev/maven-aws-ecr-k8s:1.4.0-mono . 
    docker run      svgdev/maven-aws-ecr-k8s:1.4.0-mono mvn -v
    docker push svgdev/maven-aws-ecr-k8s:1.4.0-mono  
